module gitlab.com/Akiyamov/telegramrepost

go 1.21.3

require (
	github.com/SevereCloud/vksdk/v2 v2.16.1
	github.com/joho/godotenv v1.5.1
)

require (
	github.com/klauspost/compress v1.17.2 // indirect
	github.com/vmihailenco/msgpack/v5 v5.4.1 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	golang.org/x/text v0.13.0 // indirect
)
